use SuperheroDb

insert into Superpower(PowerName, PowerDescription)
values('rich', 'inheritance')
insert into Superpower(PowerName, PowerDescription)
values('fly', 'can fly')
insert into Superpower(PowerName, PowerDescription)
values('gills', 'can breath under water')
insert into Superpower(PowerName, PowerDescription)
values('laser vision', 'can shoot lasers from the eyes')

insert into SuperheroSuperpower(SuperheroID, SuperpowerID)
values(1, 1)
insert into SuperheroSuperpower(SuperheroID, SuperpowerID)
values(2, 2)
insert into SuperheroSuperpower(SuperheroID, SuperpowerID)
values(2, 4)
insert into SuperheroSuperpower(SuperheroID, SuperpowerID)
values(3, 3)
insert into SuperheroSuperpower(SuperheroID, SuperpowerID)
values(3, 1)