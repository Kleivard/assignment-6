use SuperheroDb

insert into Superhero (SuperheroName, Alias, Origin)
values('Batman', 'Bruce Wayne', 'Gotham');
insert into Superhero (SuperheroName, Alias, Origin)
values('Superman', 'Clark Kent', 'Krypton');
insert into Superhero (SuperheroName, Alias, Origin)
values('Aquaman', 'Arthur Curry', 'Gotham');