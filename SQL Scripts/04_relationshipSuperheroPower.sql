use SuperheroDb

create table SuperheroSuperpower(
	SuperheroID int constraint FK_Superpower_Superhero foreign key references Superhero (ID),
	SuperpowerID int constraint FK_Superhero_Superpower foreign key references Superpower (ID)

	constraint SuperheroSuperpowerID primary key (SuperheroID, SuperpowerID)
)