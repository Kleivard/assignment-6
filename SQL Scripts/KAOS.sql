USE Chinook

SELECT C.FirstName, C.LastName, TILGI.GenreName AS FavouriteGenre, TILGI.NumberOfTracks
FROM Customer AS C
INNER JOIN (
	SELECT I.CustomerId, TILG.GenreName, COUNT(TILG.GenreId) AS NumberOfTracks
	FROM Invoice AS I
	INNER JOIN (
		SELECT IL.InvoiceId, IL.TrackId, T.GenreId, G.Name AS GenreName
		FROM Track AS T, InvoiceLine AS IL, Genre AS G
		WHERE T.TrackId = IL.TrackId AND G.GenreId = T.GenreId)
	AS TILG
	ON TILG.InvoiceId = I.InvoiceId
	GROUP BY I.CustomerId, TILG.GenreName)
AS TILGI
ON TILGI.CustomerId = C.CustomerId
ORDER BY C.CustomerId, NumberOfTracks DESC