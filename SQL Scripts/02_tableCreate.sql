use SuperheroDb

create table Superhero(
	ID				int primary key identity(1,1),
	SuperheroName	varchar(50),
	Alias			varchar(25),
	Origin			varchar(25) 
)
 
create table Assistant(
	ID int primary key identity(1,1),
	AssistantName varchar(25)
)

create table Superpower(
	ID int primary key identity(1,1),
	PowerName varchar(25),
	PowerDescription varchar(255)
)
