﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_6.Repositories;
using Assignment_6.Models;

namespace Assignment_6
{
    public class SQLQueryHelper
    {
        // CUSTOMERS
        /// <summary>
        /// Used to retrive all Customer objects.
        /// </summary>
        /// <param name="customerRepository">Class to implementation of function</param>
        public void GetAllCustomers(ICustomerRepository customerRepository)
        {
            PrintCustomers(customerRepository.GetAllCustomer());
        }
        /// <summary>
        /// Print all the Customers from the database based on a offset and limit.
        /// </summary>
        /// <param name="customerRepository">Class to implementation of function</param>
        /// <param name="offset">What datapoint to start fetching data.</param>
        /// <param name="limit">Amount of rows to retrive.</param>
        public void GetCustomersByPage(ICustomerRepository customerRepository, int offset, int limit)
        {
            PrintCustomers(customerRepository.GetCustomersByPage(offset, limit));
        }
        /// <summary>
        /// Print the first matching Customers by their first name or last name. Uses LIKE 'name%' syntax.
        /// </summary>
        /// <param name="customerRepository">Class to implementation of function</param>
        /// <param name="name">Full name or part of name of Customer.</param>
        /// <returns>Returns the first match of based on argument name.</returns>
        public Customer GetCustomerByName(ICustomerRepository customerRepository, string name) 
        {
            PrintCustomer(customerRepository.GetCustomer(name));
            return customerRepository.GetCustomer(name);
        }
        /// <summary>
        /// Retrives a uniqe Customer based on ID.
        /// </summary>
        /// <param name="customerRepository">Class to implementation of function</param>
        /// <param name="ID">ID of customer UNIQE.</param>
        /// <returns>Returns a Customer.</returns>
        public Customer GetCustomerById(ICustomerRepository customerRepository, int id)
        {
            PrintCustomer(customerRepository.GetCustomer(id));
            return customerRepository.GetCustomer(id);
        }
        /// <summary>
        /// Adds a customer to the database.
        /// </summary>
        /// <param name="customer">Customer need to contain: FirstName, LastName, Country
        /// , PostalCode, Phone, and Email.</param>
        /// <returns>Return true if customer got inserted to the database.</returns>
        public bool AddCustomer(ICustomerRepository customerRepository, Customer customer)
        {
            if (customerRepository.AddCustomer(customer))
                return true;
            return false;
        }
        /// <summary>
        /// Updating a customer based on the ID that is set in the argument Customer.
        /// </summary>
        /// <param name="customer">Customer need to contain: FirstName, LastName, Country
        /// , PostalCode, Phone, and Email.</param>
        /// <returns>Returns true if Customer got updated successfully.</returns
        public bool UpdateCustomer(ICustomerRepository customerRepository, Customer customer)
        {

            if (customerRepository.UpdateCustomer(customer))
                return true;
            return false;
        }
        /// <summary>
        /// Helper function to print information about a IEnumerable of Customers.
        /// </summary>
        /// <param name="customers">IEnumerable of customer.</param>
        private void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }
        /// <summary>
        /// Helper function to print information about a Customers.
        /// Used by PrintCustomers or alone based on need.
        /// </summary>
        /// <param name="customers">Customer object</param>
        private void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"" +
                $"{customer.CustomerId}\t " +
                $"{customer.FirstName}\t" +
                $"{customer.LastName}\t" +
                $"{customer.Country}\t" +
                $"{customer.PostalCode}\t" +
                $"{customer.Phone}\t" +
                $"{customer.Email}\t"
                );
        }




        // COUNTRY
        /// <summary>
        /// Get the countries and the number of customers in each country ordered by the country with the most customers.
        /// </summary>
        /// <param name="customerCountryRepository"></param>
        public void GetCustomersByCountries(ICustomerCountryRepository customerCountryRepository)
        {
            PrintCustomersByCountries(customerCountryRepository.GetCustomersByCountries());
        }
        /// <summary>
        /// Print the countries and their customers.
        /// </summary>
        /// <param name="customerCountries"></param>
        private void PrintCustomersByCountries(IEnumerable<CustomerCountry> customerCountries)
        {
            foreach (CustomerCountry customerCountry in customerCountries)
            {
                PrintCustomersByCountry(customerCountry);
            }
        }
        /// <summary>
        /// Print a country and their customers.
        /// </summary>
        /// <param name="customerCountry"></param>
        private void PrintCustomersByCountry(CustomerCountry customerCountry)
        {
            Console.WriteLine($"" +
                $"{customerCountry.Country}\t" +
                $"{customerCountry.NumberOfCustomers}\t"
                );
        }

        // SPENDERS
        /// <summary>
        /// Get every customer who has bought something ordered by highest spender.
        /// </summary>
        /// <param name="customerSpenderRepository"></param>
        public void GetHighestSpendingCustomer(ICustomerSpenderRepository customerSpenderRepository)
        {
            PrintCustomerSpenders(customerSpenderRepository.GetHighestSpendingCustomer());
        }
        /// <summary>
        /// Print the customers and their total amount of money spent.
        /// </summary>
        /// <param name="customerSpenders"></param>
        private void PrintCustomerSpenders(IEnumerable<CustomerSpender> customerSpenders)
        {
            foreach (CustomerSpender customerSpender in customerSpenders)
            {
                PrintCustomerSpender(customerSpender);
            }
        }
        /// <summary>
        /// Print a customer and their total amount of money spent.
        /// </summary>
        /// <param name="customerSpender"></param>
        private void PrintCustomerSpender(CustomerSpender customerSpender)
        {
            Console.WriteLine($"" +
                $"{customerSpender.FirstName}\t" +
                $"{customerSpender.LastName}\t" +
                $"{customerSpender.Total}\t"
                );
        }

        // GENRE
        /// <summary>
        /// Get the customer's most popular genre(s)
        /// </summary>
        /// <param name="customerGenreRepository"></param>
        /// <param name="ID"></param>
        public void GetTopGenres(ICustomerGenreRepository customerGenreRepository, int ID)
        {
            PrintGetTopGenre(customerGenreRepository.GetTopGenres(ID));
        }
        /// <summary>
        /// Print the customer and theri most popular genre(s)
        /// </summary>
        /// <param name="customerGenre"></param>
        private void PrintGetTopGenre(CustomerGenre customerGenre)
        {
            StringBuilder sb = new();
            sb.Append(customerGenre.FirstName + "\t");
            sb.Append(customerGenre.LastName + "\t");

            foreach (string genre in customerGenre.TopGenres)
            {
                sb.Append(genre + "\t");
            }
            Console.WriteLine(sb.ToString());
        }
    }
}
