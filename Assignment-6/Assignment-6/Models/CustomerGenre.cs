﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_6.Models
{
    public class CustomerGenre
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> TopGenres { get; set; } = new();
    }
}
