﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_6.Models
{
    public class CustomerCountry
    {
        public string Country { get; set; }

        public int NumberOfCustomers { get; set; }
    }
}
