﻿using Assignment_6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_6.Repositories
{
    /// <summary>
    /// Used to query Customer table from Chinook database.
    /// Implements ICustomerRepository.
    /// </summary>
    public class CustomerRepository : ICustomerRepository
    {
        public bool AddCustomer(Customer customer)
        {
            string SQL_QUERY =
                "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                $"VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        sqlCommand.Parameters.AddWithValue("@LastName", customer.LastName);
                        sqlCommand.Parameters.AddWithValue("@Country", customer.Country);
                        sqlCommand.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        sqlCommand.Parameters.AddWithValue("@Phone", customer.Phone);
                        sqlCommand.Parameters.AddWithValue("@Email", customer.Email);
                        Console.WriteLine($"Rows effected: {sqlCommand.ExecuteNonQuery()}"); 
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }

        public List<Customer> GetAllCustomer()
        {
            List<Customer> customerList = new();
            string SQL_QUERY =
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer";

            try
            {
                //Connection
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    //Make a command
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        //Reader
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            //Handle result
                            while (sqlDataReader.Read())
                            {
                                Customer customerTemp = new();
                                customerTemp.CustomerId = sqlDataReader.GetInt32(0);
                                customerTemp.FirstName = sqlDataReader.GetString(1);
                                customerTemp.LastName = sqlDataReader.GetString(2);
                                customerTemp.Country = sqlDataReader.GetString(3);
                                customerTemp.PostalCode = sqlDataReader.IsDBNull(4) ? null : sqlDataReader.GetString(4);
                                customerTemp.Phone = sqlDataReader.IsDBNull(5) ? null : sqlDataReader.GetString(5);
                                customerTemp.Email = sqlDataReader.GetString(6);
                                customerList.Add(customerTemp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }

        public List<Customer> GetCustomersByPage(int offset, int limit)
        {
            List<Customer> customerList = new();
            string SQL_QUERY =
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "ORDER BY Customer.CustomerId " +
                $"OFFSET {offset} ROWS " +
                $"FETCH FIRST {limit} ROWS ONLY";
            try
            {
                //Connection
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    //Make a command
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        //Reader
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            //Handle result
                            while (sqlDataReader.Read())
                            {
                                Customer customerTemp = new();
                                customerTemp.CustomerId = sqlDataReader.GetInt32(0);
                                customerTemp.FirstName = sqlDataReader.GetString(1);
                                customerTemp.LastName = sqlDataReader.GetString(2);
                                customerTemp.Country = sqlDataReader.GetString(3);
                                customerTemp.PostalCode = sqlDataReader.IsDBNull(4) ? null : sqlDataReader.GetString(4);
                                customerTemp.Phone = sqlDataReader.IsDBNull(5) ? null : sqlDataReader.GetString(5);
                                customerTemp.Email = sqlDataReader.GetString(6);
                                customerList.Add(customerTemp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }

        public Customer GetCustomer(string name)
        {
            Customer customer = new();
            string SQL_QUERY =
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                $"WHERE FirstName LIKE '{name}%' OR LastName LIKE '{name}%'";

            try
            {
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.Read())
                            {
                                customer.CustomerId = sqlDataReader.GetInt32(0);
                                customer.FirstName = sqlDataReader.GetString(1);
                                customer.LastName = sqlDataReader.GetString(2);
                                customer.Country = sqlDataReader.GetString(3);
                                customer.PostalCode = sqlDataReader.IsDBNull(4) ? null : sqlDataReader.GetString(4);
                                customer.Phone = sqlDataReader.IsDBNull(5) ? null : sqlDataReader.GetString(5);
                                customer.Email = sqlDataReader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex); ;
            }
            return customer;
        }

        public Customer GetCustomer(int ID)
        {
            Customer customer = new();
            string SQL_QUERY =
                "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                $"WHERE CustomerId = {ID}";
            try
            {
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.Read())
                            {
                                customer.CustomerId = sqlDataReader.GetInt32(0);
                                customer.FirstName = sqlDataReader.GetString(1);
                                customer.LastName = sqlDataReader.GetString(2);
                                customer.Country = sqlDataReader.GetString(3);
                                customer.PostalCode = sqlDataReader.IsDBNull(4) ? null : sqlDataReader.GetString(4);
                                customer.Phone = sqlDataReader.IsDBNull(5) ? null : sqlDataReader.GetString(5);
                                customer.Email = sqlDataReader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex); ;
            }
            return customer;
        }

        public bool UpdateCustomer(Customer customer)
        {
            string SQL_QUERY =
                "UPDATE Customer " +
                $"SET FirstName = @FirstName, LastName = @LastName, Country = @Country," +
                $" PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                $"WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        sqlCommand.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        sqlCommand.Parameters.AddWithValue("@LastName", customer.LastName);
                        sqlCommand.Parameters.AddWithValue("@Country", customer.Country);
                        sqlCommand.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        sqlCommand.Parameters.AddWithValue("@Phone", customer.Phone);
                        sqlCommand.Parameters.AddWithValue("@Email", customer.Email);
                        sqlCommand.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        Console.WriteLine($"Rows effected: {sqlCommand.ExecuteNonQuery()}");
                        
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            return true;
        }
    }
}
