﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_6.Models;

namespace Assignment_6.Repositories
{
    /// <summary>
    /// Used by CustomerRepository, contains method for querying for Customer.
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// Retrives all the Customers from the database.
        /// </summary>
        /// <returns>Returns a List with Customers.</returns>
        public List<Customer> GetAllCustomer();
        /// <summary>
        /// Retrives all the Customers from the database based on a offset and limit.
        /// </summary>
        /// <param name="offset">What datapoint to start fetching data.</param>
        /// <param name="limit">Amount of rows to retrive.</param>
        /// <returns>Returns a List with Customers based on offset and limit(page).</returns>
        public List<Customer> GetCustomersByPage(int offset, int limit);
        /// <summary>
        /// Get a Customer by their first name or last name. Uses LIKE 'name%' syntax.
        /// </summary>
        /// <param name="name">Full name or part of name of Customer.</param>
        /// <returns>Returns the first match of based on argument name.</returns>
        public Customer GetCustomer(string name);
        /// <summary>
        /// Retrives a uniqe Customer based on ID.
        /// </summary>
        /// <param name="ID">ID of customer UNIQE.</param>
        /// <returns>Returns a Customer.</returns>
        public Customer GetCustomer(int ID);
        /// <summary>
        /// Adds a customer to the database.
        /// </summary>
        /// <param name="customer">Customer need to contain: FirstName, LastName, Country
        /// , PostalCode, Phone, and Email.</param>
        /// <returns>Return true if customer got inserted to the database.</returns>
        public bool AddCustomer(Customer customer);
        /// <summary>
        /// Updating a customer based on the ID that is set in the argument Customer.
        /// </summary>
        /// <param name="customer">Customer need to contain: FirstName, LastName, Country
        /// , PostalCode, Phone, and Email.</param>
        /// <returns>Returns true if Customer got updated successfully.</returns>
        public bool UpdateCustomer(Customer customer);
    }
}
