﻿using Assignment_6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_6.Repositories
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {

        public List<CustomerCountry> GetCustomersByCountries()
        {
            List<CustomerCountry> customersByCountries = new();
            string SQL_QUERY =
                "SELECT Country, COUNT(CustomerId) " +
                "FROM Customer " +
                "GROUP BY Country " +
                "ORDER BY COUNT(CustomerId) DESC, Country ASC";
            try
            {
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            while (sqlDataReader.Read())
                            {
                                CustomerCountry customerCountry = new();
                                customerCountry.Country = sqlDataReader.GetString(0);
                                customerCountry.NumberOfCustomers = sqlDataReader.GetInt32(1);
                                customersByCountries.Add(customerCountry);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customersByCountries;
        }
    }
}
