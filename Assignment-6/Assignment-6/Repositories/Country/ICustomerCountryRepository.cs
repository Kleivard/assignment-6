﻿using Assignment_6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_6.Repositories
{
    public interface ICustomerCountryRepository
    {
        /// <summary>
        /// Gets the countries and the number of customers in each country ordered by the country with the most customers.
        /// </summary>
        /// <returns>Return a List of type CustomerCountry.</returns>
        public List<CustomerCountry> GetCustomersByCountries();
    }
}
