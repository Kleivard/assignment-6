﻿using Assignment_6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_6.Repositories
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        public CustomerGenre GetTopGenres(int ID)
        {
            CustomerGenre customerGenre = new();
            string SQL_QUERY =
                "SELECT C.FirstName, C.LastName, G.Name " +
                "FROM Genre AS G " +
                "JOIN(" +
                    "SELECT TOP 1 WITH TIES C.CustomerId, T.GenreId, COUNT(T.GenreID) AS Tracks " +
                    "FROM Customer AS C " +
                    "JOIN Invoice AS I " +
                        $"ON I.CustomerId = {ID} AND C.CustomerId = {ID} " +
                    "JOIN InvoiceLine AS IL " +
                        "ON IL.InvoiceId = I.InvoiceId " +
                    "JOIN Track AS T " +
                        "ON T.TrackId = IL.TrackId " +
                    "GROUP BY C.CustomerId, T.GenreId " +
                    "ORDER BY C.CustomerId, Tracks DESC) " +
                    "AS Info ON G.GenreId = Info.GenreId " +
                "JOIN Customer AS C " +
                    "ON C.CustomerId = Info.CustomerId";

            try
            {
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            if (sqlDataReader.Read())
                            {
                                customerGenre.FirstName = sqlDataReader.GetString(0);
                                customerGenre.LastName = sqlDataReader.GetString(1);
                                customerGenre.TopGenres.Add(sqlDataReader.GetString(2));
                                while (sqlDataReader.Read())
                                {
                                    customerGenre.TopGenres.Add(sqlDataReader.GetString(2));
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerGenre;
        }
    }
}
