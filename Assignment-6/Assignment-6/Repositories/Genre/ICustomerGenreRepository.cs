﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_6.Models;

namespace Assignment_6.Repositories
{
    public interface ICustomerGenreRepository
    {
        /// <summary>
        /// Get the customer's most popular genre(s)
        /// </summary>
        /// <param name="ID"></param>
        /// <returns>Returns a CustomerGenre.</returns>
        public CustomerGenre GetTopGenres(int ID);
    }
}
