﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_6.Models;

namespace Assignment_6.Repositories
{
    public interface ICustomerSpenderRepository
    {
        /// <summary>
        /// Get every customer who has bought something ordered by highest spender.
        /// </summary>
        /// <returns>Returns a List of type CustomerSpender.</returns>
        public List<CustomerSpender> GetHighestSpendingCustomer();
    }
}
