﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assignment_6.Models;

namespace Assignment_6.Repositories
{
    public class CustomerSpenderRepository: ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetHighestSpendingCustomer()
        {
            List<CustomerSpender> customerSpenders = new();
            string SQL_QUERY =
                "SELECT C.FirstName, C.LastName , TotalInvoice.total_invoice " +
                "FROM Customer AS C " +
                "INNER JOIN " +
                    "(SELECT I.CustomerId, SUM(Total) AS total_invoice " +
                    "FROM Invoice AS I " +
                    "GROUP BY I.CustomerId " +
                    ") AS TotalInvoice " +
                "ON TotalInvoice.CustomerId = C.CustomerId " +
                "ORDER BY TotalInvoice.total_invoice desc;";


            try
            {
                using (SqlConnection sqlConnection = new(ConnectionHelper.GetConnectionString()))
                {
                    sqlConnection.Open();
                    using (SqlCommand sqlCommand = new(SQL_QUERY, sqlConnection))
                    {
                        using (SqlDataReader sqlDataReader = sqlCommand.ExecuteReader())
                        {
                            while (sqlDataReader.Read())
                            {
                                CustomerSpender customerSpender = new();
                                customerSpender.FirstName = sqlDataReader.GetString(0);
                                customerSpender.LastName = sqlDataReader.GetString(1);
                                customerSpender.Total = sqlDataReader.GetDecimal(2);
                                customerSpenders.Add(customerSpender);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerSpenders;
        }
    }
}
