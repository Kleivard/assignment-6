﻿using System;
using Assignment_6.Repositories;
using Assignment_6.Models;

namespace Assignment_6
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLQueryHelper queryHelper = new();
            CustomerRepository customerRepository = new();
            CustomerSpenderRepository customerSpenderRepository = new();
            CustomerCountryRepository customerCountryRepository = new();
            CustomerGenreRepository customerGenreRepository = new();
            Customer customer = new()
            {
                FirstName = "Clark",
                LastName = "Kent",
                Country = "Metropolis",
                PostalCode = "54186",
                Phone = "+46735151697",
                Email = "arthur.superman@gmail.com"
            };

            //queryHelper.GetAllCustomers(customerRepository);
            //queryHelper.GetCustomerById(customerRepository, customer.CustomerId);
            //queryHelper.GetCustomerByName(customerRepository, customer.FirstName);
            //queryHelper.GetCustomersByPage(customerRepository, 5, 10);
            //Customer customer1 = new()
            //{
            //    FirstName = "Bruce",
            //    LastName = "Wayne",
            //    Country = "Gotham",
            //    PostalCode = "54186",
            //    Phone = "+46785151697",
            //    Email = "xCoomunist@batman.com"
            //};
            //queryHelper.AddCustomer(customerRepository, customer1);
            //customer.Email = "realClarkKent@outlook.com";
            //queryHelper.UpdateCustomer(customerRepository, customer);

            //queryHelper.GetCustomersByCountries(customerCountryRepository);
            //queryHelper.GetHighestSpendingCustomer(customerSpenderRepository);
            //queryHelper.GetTopGenres(customerGenreRepository, customer.CustomerId);
        }
    }
}
